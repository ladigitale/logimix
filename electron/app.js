const fs = require('fs-extra')
const express = require('express')
const server = express()
const bodyParser = require('body-parser')
const url = require('url')
const path = require('path')
const cors = require('cors')
const multer  = require('multer')
const ffmpeg = require('fluent-ffmpeg')
const archiver = require('archiver')
const extract = require('extract-zip')
const { app, shell, BrowserWindow, Menu, MenuItem, globalShortcut, systemPreferences, screen } = require('electron')

if (app.isPackaged) {
	process.env.NODE_ENV = 'production'
	Menu.setApplicationMenu(false)
}
app.allowRendererProcessReuse = true

let mainWindow, splash
let splashscreen = true

const demarrer = async () => {
	let cheminFichiers, cheminProjets, cheminFFMPEG, cheminTemp
	const portable = false
	if (process.env.NODE_ENV === 'production' && portable === false) {
		cheminFichiers = path.normalize(app.getPath('documents') + '/Logimix/fichiers')
		cheminProjets = path.normalize(app.getPath('documents') + '/Logimix/projets')
		cheminFFMPEG = require('ffmpeg-static').replace(
			'app.asar',
			'app.asar.unpacked'
		)
		cheminTemp = path.normalize(app.getPath('documents') + '/Logimix/temp')
	} else if (process.env.NODE_ENV === 'production' && portable === true) {
		cheminFichiers = path.resolve(__dirname, '../../fichiers')
		cheminProjets = path.resolve(__dirname, '../../projets')
		cheminFFMPEG = require('ffmpeg-static').replace(
			'app.asar',
			'app.asar.unpacked'
		)
		cheminTemp = path.resolve(__dirname, '../../temp')
	} else {
		cheminFichiers = path.resolve(__dirname, './fichiers')
		cheminProjets = path.resolve(__dirname, './projets')
		cheminFFMPEG = require('ffmpeg-static')
		cheminTemp = path.resolve(__dirname, './temp')
	}

	fs.mkdirpSync(cheminFichiers)
	fs.mkdirpSync(cheminProjets)
	fs.mkdirpSync(cheminTemp)
	fs.emptyDirSync(cheminTemp)
	
	server.set('trust proxy', 1)
	server.use(cors())
	server.use('/fichiers', express.static(cheminFichiers))
	server.use('/projets', express.static(cheminProjets))
	server.use('/temp', express.static(cheminTemp))
	server.use(bodyParser.json({ limit: '1000mb' }))
	server.use(bodyParser.urlencoded({ limit: '1000mb', extended: true }))

	const listen = server.listen()
	const port = listen.address().port
	server.listen()

	app.whenReady().then(function () {
		const touchSupport = screen.getPrimaryDisplay().touchSupport
		const html = genererHTML(port, touchSupport)
		if (process.env.NODE_ENV === 'production') {
			fs.writeFileSync(path.join(app.getAppPath(), 'app/index.html').replace('app.asar', 'app.asar.unpacked'), html, 'utf8')
		} else {
			fs.writeFileSync(path.resolve(__dirname, './app/index.html'), html, 'utf8')
		}
		creerFenetre()
	})

	app.on('window-all-closed', () => {
		if (process.platform !== 'darwin') {
			app.quit()
		}
	})

	app.on('activate', () => {
		if (BrowserWindow.getAllWindows().length === 0) {
			creerFenetre()
		}
	})

	server.post('/api/enregistrer-audio', function (req, res) {
		televerser(req, res, function (err) {
			if (err) {
				res.end(err.toString().substr(7))
			} else {
				const mixage = req.body.mixage
				const fichier = req.file.filename
				const nom = fichier.replace(/\.[^/.]+$/, '')
				if (req.file.mimetype !== 'audio/mpeg' && req.file.mimetype !== 'audio/mp3') {
					new ffmpeg({ source: path.normalize(cheminFichiers + '/' + fichier) })
						.setFfmpegPath(cheminFFMPEG)
						.output(path.normalize(cheminFichiers + '/' + nom + '.mp3'))
						.audioBitrate(192)
						.audioFrequency(44100)
						.on('end', function() {
							fs.removeSync(path.normalize(cheminFichiers + '/' + fichier))
							fs.mkdirpSync(path.normalize(cheminProjets + '/' + mixage))
							fs.copy(path.normalize(cheminFichiers + '/' + nom + '.mp3'), path.normalize(cheminProjets + '/' + mixage + '/' + nom + '.mp3'), function () {
								res.send(nom + '.mp3')
							})
						})
						.run()
				} else {
					fs.mkdirpSync(path.normalize(cheminProjets + '/' + mixage))
					fs.copy(path.normalize(cheminFichiers + '/' + fichier), path.normalize(cheminProjets + '/' + mixage + '/' + fichier), function () {
						res.send(fichier)
					})
				}
			}
		})
	})

	server.post('/api/copier-audio', function (req, res) {
		const mixage = req.body.mixage
		const fichier = req.body.fichier
		fs.mkdirpSync(path.normalize(cheminProjets + '/' + mixage))
		fs.copy(path.normalize(cheminFichiers + '/' + fichier), path.normalize(cheminProjets + '/' + mixage + '/' + fichier), function () {
			res.send('audio_copie')
		})
	})

	server.post('/api/supprimer-audio', function (req, res) {
		const fichier = req.body.fichier
		if (fs.pathExistsSync(path.normalize(cheminFichiers + '/' + fichier))) {
			fs.removeSync(path.normalize(cheminFichiers + '/' + fichier))
			res.send('audio_supprime')
		} else {
			res.send('erreur')
		}
	})

	server.post('/api/importer-mixage', function (req, res) {
		televerserTemp(req, res, async function (err) {
			if (err) { res.send('erreur'); return false }
			try {
				const extension = path.extname(req.file.filename)
				const archive = path.basename(req.file.filename, extension)
				const source = path.normalize(cheminTemp + '/' + req.file.filename)
				const cible = path.normalize(cheminTemp + '/' + archive)
				await extract(source, { dir: cible })
				const donneesMixage = await fs.readJson(path.normalize(cible + '/donnees.json'))
				let fichiers = []
				donneesMixage.donnees.forEach(function (piste) {
					if (!piste.src.includes('./static/sonotheque/')) {
						const lien = url.parse(piste.src)
						const fichier = path.basename(lien.pathname)
						fichiers.push(fichier)
					}
				})
				if (fichiers.length > 0) {
					fs.mkdirpSync(path.normalize(cheminProjets + '/' + donneesMixage.id))
					fichiers.forEach(function (fichier) {
						fs.copySync(path.normalize(cible + '/fichiers/' + fichier), path.normalize(cheminProjets + '/' + donneesMixage.id + '/' + fichier))
					})
				}
				fs.removeSync(source)
				fs.removeSync(cible)
				res.json(donneesMixage)
			} catch (e) {
				if (fs.pathExistsSync(path.normalize(cheminTemp + '/' + req.file.filename))) {
					fs.removeSync(path.normalize(cheminTemp + '/' + req.file.filename))
					res.send('erreur')
				} else {
					res.send('erreur')
				}
			}
		})
	})

	server.post('/api/exporter-mixage', function (req, res) {
		const mixage = req.body.mixage
		const donneesMixage = req.body.donnees
		fs.mkdirpSync(path.normalize(cheminTemp + '/' + mixage))
		fs.mkdirpSync(path.normalize(cheminTemp + '/' + mixage + '/fichiers'))
		fs.writeFileSync(path.normalize(cheminTemp + '/' + mixage + '/donnees.json'), JSON.stringify(donneesMixage, '', 4), 'utf8')
		donneesMixage.donnees.forEach(function (piste) {
			if (!piste.src.includes('./static/sonotheque/')) {
				const lien = url.parse(piste.src)
				const fichier = path.basename(lien.pathname)
				fs.copySync(path.normalize(cheminProjets + '/' + mixage + '/' + fichier), path.normalize(cheminTemp + '/' + mixage + '/fichiers/' + fichier))
			}
		})
		const archiveId = Math.floor((Math.random() * 100000) + 1)
		const sortie = fs.createWriteStream(path.normalize(cheminTemp + '/' + mixage + '.zip'))
		const archive = archiver('zip', {
			zlib: { level: 9 }
		})
		sortie.on('finish', async function () {
			await fs.remove(path.normalize(cheminTemp + '/' + mixage))
			res.send('mixage_exporte')
		})
		archive.pipe(sortie)
		archive.directory(path.normalize(cheminTemp + '/' + mixage), false)
		archive.finalize()
	})

	server.post('/api/supprimer-mixage', function (req, res) {
		const mixage = req.body.mixage
		if (fs.pathExistsSync(path.normalize(cheminProjets + '/' + mixage))) {
			fs.removeSync(path.normalize(cheminProjets + '/' + mixage))
			res.send('mixage_supprime')
		} else {
			res.send('erreur')
		}
	})

	const televerser = multer({
		storage: multer.diskStorage({
			destination: function (req, fichier, callback) {
				callback(null, cheminFichiers)
			},
			filename: function (req, fichier, callback) {
				const extension = path.extname(fichier.originalname).toLowerCase()
				const nom = 'audio_' + new Date().getTime() + extension
				callback(null, nom)
			}
		})
	}).single('fichier')

	const televerserTemp = multer({
		storage: multer.diskStorage({
			destination: function (req, fichier, callback) {
				callback(null, cheminTemp)
			},
			filename: function (req, fichier, callback) {
				const extension = path.extname(fichier.originalname).toLowerCase()
				const nom = 'archive_' + new Date().getTime() + extension
				callback(null, nom)
			}
		})
	}).single('fichier')

	function genererHTML (port, touchSupport) {
		let js = ''
		let css = ''
		fs.readdirSync(path.resolve(__dirname, './app/static/assets')).forEach(fichier => {
			if (fichier.includes('.js')) {
				js = fichier
			} else if (fichier.includes('.css')) {
				css = fichier
			}
		})
		const html = `<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
				<title>Logimix by La Digitale</title>
				<link rel="icon" type="image/png" href="./static/favicon.png">
				<script type="module" crossorigin src="./static/assets/` + js + `"></script>
				<link rel="stylesheet" href="./static/assets/` + css + `">
			</head>
			<body>
				<div id="app" data-port="` + port + `" data-touchscreen="` + touchSupport + `"></div>
				<script src="./static/js/waveform-playlist.js"></script>
			</body>
		</html>`
		return html
	}

	function creerFenetre () {
		const dimensions = screen.getPrimaryDisplay().size
		let largeur = dimensions.width - 400
		let hauteur = dimensions.height - 150
		if (largeur < 800) {
			largeur = 800
		}
		if (hauteur < 600) {
			hauteur = 600
		}
		mainWindow = new BrowserWindow({
			minWidth: 800,
			minHeight: 600,
			width: largeur,
			height: hauteur,
			center: true,
			icon: path.resolve(__dirname, './app/static/icon.png'),
			webPreferences: {
				nativeWindowOpen: true,
				contextIsolation: true,
				sandbox: false
			}
		})

		mainWindow.loadURL(`file://${__dirname}/app/index.html`)

		if (splashscreen !== false) {
			splash = new BrowserWindow({
				parent: mainWindow,
				width: 350,
				height: 350,
				transparent: true,
				center: true,
				frame: false,
				alwaysOnTop: true,
				icon: path.resolve(__dirname, './app/static/icon.png'),
				webPreferences: {
					nativeWindowOpen: true,
					contextIsolation: true,
					sandbox: false
				}
			})
			splash.loadURL(`file://${__dirname}/app/splash.html`)
			splash.show()
			mainWindow.show()
		} else {
			mainWindow.show()
			mainWindow.focus()
		}

		setTimeout(() => {
			if (splashscreen === true) {
				splash.close()
			}
			splashscreen = false
			mainWindow.focus()
		}, 1500)

		mainWindow.webContents.setWindowOpenHandler(({ url }) => {
			shell.openExternal(url)
			return { action: 'deny' }
		})

		mainWindow.webContents.on('context-menu', (_, props) => {
			const menu = new Menu()
			if (props.isEditable) {
				menu.append(new MenuItem({ label: 'Couper', role: 'cut' }))
				menu.append(new MenuItem({ label: 'Copier', role: 'copy' }))
				menu.append(new MenuItem({ label: 'Coller', role: 'paste' }))
				menu.popup()
			}
		})

		if (process.platform === 'darwin') {
			mainWindow.on('focus', () => {
				if (mainWindow.isFocused()) {
					globalShortcut.register('CommandOrControl+C', () => {
						mainWindow.webContents.copy()
					})

					globalShortcut.register('CommandOrControl+V', () => {
						mainWindow.webContents.paste()
					})

					globalShortcut.register('CommandOrControl+X', () => {
						mainWindow.webContents.cut()
					})
				}
			})

			mainWindow.on('blur', () => {
				globalShortcut.unregister('CommandOrControl+C')
				globalShortcut.unregister('CommandOrControl+V')
				globalShortcut.unregister('CommandOrControl+X')
			})

			systemPreferences.askForMediaAccess('microphone').then(function (granted) {
				if (!granted) {
					alert('Vous ne pourrez pas enregistrer votre voix si l\'accès au microphone n\'est pas autorisé.')
				}
			})
		}
	}
}

demarrer()
