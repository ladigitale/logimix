# Logimix

Logimix est un logiciel pour créer des mixages audio hors ligne. 
Les sons intégrés sont libres de droits et proviennent du site La Sonothèque (https://lasonotheque.org).
Logimix utilise Waveform-playlist de Naomi Aro pour le rendu et le traitement des fichiers audio (https://github.com/naomiaro/waveform-playlist).

Le logiciel est publié sous licence GNU GPLv3.
Sauf les fontes Roboto, Roboto Slab et Material Icons (Apache License Version 2.0), la fonte HKGrotesk (Sil Open Font Licence 1.1) et le ficher waveform-playlist.js (MIT License)
## Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run dev
```

### Compilation et minification des fichiers
```
npm run build

Les fichiers sont compilés dans le dossier Electron/app
```

### Préparation du projet Electron
```
cd electron && npm install
```

### Démarrage du logiciel
```
npm run start
```

### Compilation du logiciel
```
pour Windows : npm run dist:win
pour macOS : npm run dist:mac (fichier provisionprofile nécessaire)
pour GNU/Linux : npm run dist:linux
```

### Présentation
https://ladigitale.dev/logimix/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

