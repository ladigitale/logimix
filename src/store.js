import { createStore } from 'vuex'
import storage from 'vuex-persistedstate'

export default createStore({
	state: {
		version: '1.1.0',
		chemin: '',
		chargement: true,
		message: '',
		information: '',
		mixages: [],
		fichiers: []
	},
	mutations: {
		modifierChemin (state, chemin) {
			state.chemin = chemin
		},
		modifierChargement (state, boolean) {
			state.chargement = boolean
		},
		modifierMessage (state, message) {
			state.message = message
		},
		modifierInformation (state, information) {
			state.information = information
		},
		modifierVersion (state, version) {
			state.version = version
		},
		enregistrerMixage (state, mixage) {
			const id = mixage.id
			if (state.mixages.map(function (e) { return e.id }).indexOf(id) === -1) {
				state.mixages.push(mixage)
			} else {
				const index = state.mixages.map(function (e) { return e.id }).indexOf(id)
				state.mixages[index] = mixage
			}
		},
		modifierTitreMixage (state, donnees) {
			const id = donnees.id
			const index = state.mixages.map(function (e) { return e.id }).indexOf(id)
			state.mixages[index].titre = donnees.titre
		},
		supprimerMixage (state, index) {
			state.mixages.splice(index, 1)
		},
		ajouterFichier (state, donnees) {
			state.fichiers.push(donnees)
		},
		supprimerFichier (state, index) {
			state.fichiers.splice(index, 1)
		}
	},
	actions: {
		modifierChemin ({ commit }, chemin) {
			commit('modifierChemin', chemin)
		},
		modifierChargement ({ commit }, boolean) {
			commit('modifierChargement', boolean)
		},
		modifierMessage ({ commit }, message) {
			commit('modifierMessage', message)
		},
		modifierInformation ({ commit }, information) {
			commit('modifierInformation', information)
		},
		modifierVersion ({ commit }, version) {
			commit('modifierVersion', version)
		},
		enregistrerMixage ({ commit }, donnees) {
			commit('enregistrerMixage', donnees)
		},
		modifierTitreMixage ({ commit }, donnees) {
			commit('modifierTitreMixage', donnees)
		},
		supprimerMixage ({ commit }, index) {
			commit('supprimerMixage', index)
		},
		ajouterFichier ({ commit }, donnees) {
			commit('ajouterFichier', donnees)
		},
		supprimerFichier ({ commit }, index) {
			commit('supprimerFichier', index)
		}
	},
	plugins: [storage({ key: 'logimix', storage: window.localStorage })]
})
