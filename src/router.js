import { createRouter, createWebHashHistory } from 'vue-router'
import Accueil from './views/Accueil.vue'
import Mixage from './views/Mixage.vue'

const routes = [
	{
		path: '/',
		name: 'Accueil',
		component: Accueil
	},
	{
		path: '/mixage/:id',
		name: 'Mixage',
		component: Mixage
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router
